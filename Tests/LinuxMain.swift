import XCTest

import MAFTransactionTests

var tests = [XCTestCaseEntry]()
tests += MAFTransactionTests.allTests()
XCTMain(tests)
