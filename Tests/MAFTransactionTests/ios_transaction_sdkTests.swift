//
//  ios_transaction_sdkTests.swift
//  ios-transaction-sdkTests
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import XCTest
@testable import MAFTransaction

class ios_transaction_sdkTests: XCTestCase {
    var sessionProvider: URLSessionProvider!
    var tAction: Transactions!
    
    override func setUp() {
         sessionProvider = URLSessionProvider()
         MAFTransactionSdk.start(with: MAFTransactionSdkConfiguration(accessToken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlFrVkVORGM0TlRWRVJqQXlORVUxUmtNNU9UUkRRVFJHTnpWRlF6UkNRVU01TURaQ01VRXpOUSJ9.eyJodHRwczovL21hZi5ncmF2dHkuYXV0aC9kZXYvYXBpL21lbWJlcklkIjoiOTQ3ODQwMDAwMDM2NjUzMCIsImh0dHBzOi8vbWFmLmdyYXZ0eS5hdXRoL2Rldi9hcGkvdm94TWVtYmVySWQiOiJTSEFSRTc5MzNCMlQ2OE4iLCJodHRwczovL21hZi5ncmF2dHkuYXV0aC9kZXYvYXBpL3ZveENhcmROdW1iZXIiOiI5NDc4NDAwMDAwMzY2NTMwIiwiaHR0cHM6Ly9tYWYuaWRlbnRpdHkuYXV0aC9kZXYvYXBpL2VtYWlsIjoibWVAaWh1c25pLmNvbSIsImh0dHBzOi8vbWFmLmlkZW50aXR5LmF1dGgvZGV2L2FwaS9yaWQiOm51bGwsImlzcyI6Imh0dHBzOi8vc2l0Lm1hZi1kZXYuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVkYjAxMzM4OWY2NWJiNDRiNTg1NDNhMCIsImF1ZCI6WyJodHRwczovL3NpdC5tYWYtZGV2LmF1dGgwLmNvbS9hcGkvdjIvIiwiaHR0cHM6Ly9zaXQubWFmLWRldi5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTc5MDk2MDY2LCJleHAiOjE1ODAzMDU2NjYsImF6cCI6IjJJR1ZVc1RwaWQwTlpoamFNVWU3N2s2d0VScFowcGJHIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCBhZGRyZXNzIHBob25lIHJlYWQ6Y3VycmVudF91c2VyIHVwZGF0ZTpjdXJyZW50X3VzZXJfbWV0YWRhdGEgZGVsZXRlOmN1cnJlbnRfdXNlcl9tZXRhZGF0YSBjcmVhdGU6Y3VycmVudF91c2VyX21ldGFkYXRhIGNyZWF0ZTpjdXJyZW50X3VzZXJfZGV2aWNlX2NyZWRlbnRpYWxzIGRlbGV0ZTpjdXJyZW50X3VzZXJfZGV2aWNlX2NyZWRlbnRpYWxzIHVwZGF0ZTpjdXJyZW50X3VzZXJfaWRlbnRpdGllcyIsImd0eSI6InBhc3N3b3JkIn0.wW9UluJMT48vbkROD50xXp3OClEfDnUb0oWSfaWwR-zWwuxWodQikpQT9qfNIuTk2OIr4_9IN1sATWhGHo2WuT862axzPqYzVXmuVjDaD6uHHCo0xIdCJPz8nn3fZL4GZGhO8LdDyZyLMG9niIqWY5CJm2Pgz_Gu_vidSODPrzIm1UrThha_gBB1ynTvxPzTVnDoWHHgd4zg_Xao_n8OdAMIOtbKA--YSYLp52i_QY7xKFINkTIqHkG4bAg2nvlBILJbQ9NWx1DJTMxCvkuqodGFs20x_axchFfT2I6yCT3vkLDZ_w_x1FQjL2zyVb2JF-tXUwAGwlxBTV8wPB7sow", xApiKey: "GfqP7b2I99sUMkbxGEk5Xk56RscaWRuo"))
    }
    func test_call_list_of_Transactions(){
        MAFTransactionSdk.getTransactionList(page: 5) { (transactionList, error) in
            if error != nil{
                XCTAssertFalse(true)
            }else{
                self.tAction = transactionList
                XCTAssertFalse(self.tAction.transactions?.isEmpty ?? true)
            }
        }
        let exp = expectation(description: "Test after 5 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 7.0)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertFalse(self.tAction.transactions?.isEmpty ?? true)
        } else {
            XCTFail("Delay interrupted")
        }
    }
    
    func test_call_next_not_nil_list_of_Transactions(){
        sessionProvider.request(type: Transactions.self, service: TransactionService.transactionList(page: 5, cursor: "")) { response in
            switch response {
            case let .success(transactionList):
                self.tAction = transactionList
                XCTAssertTrue((self.tAction.next != nil))
            case let .apiFailure(error):
                print(error)
                XCTAssertFalse(true)
            case let .failure(error):
                print(error)
                XCTAssertFalse(true)
            }
        }
        let exp = expectation(description: "Test after 5 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 7.0)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertTrue((self.tAction.next != nil))
        } else {
            XCTFail("Delay interrupted")
        }
    }
    func test_call_next_list_of_Transactions(){
        MAFTransactionSdk.getNextTransactionList(page: 5) { (transactionList, error) in
            if error != nil{
                XCTAssertFalse(true)
            }else{
                self.tAction = transactionList
                XCTAssertFalse(self.tAction.transactions?.isEmpty ?? true)
            }
        }
        let exp = expectation(description: "Test after 5 seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: 7.0)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertFalse(self.tAction.transactions?.isEmpty ?? true)
        } else {
            XCTFail("Delay interrupted")
        }
    }
}
