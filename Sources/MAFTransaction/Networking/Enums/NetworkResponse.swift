//
//  NetworkResponse.swift
//  ios-transaction-sdk
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import Foundation
enum NetworkResponse<T> {
    case success(T)
    case failure(NetworkError)
    case apiFailure(TransactionError)
}
