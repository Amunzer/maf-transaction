//
//  NetworkError.swift
//  ios-transaction-sdk
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import Foundation
enum NetworkError {
  case unknown
  case noJSONData
}
