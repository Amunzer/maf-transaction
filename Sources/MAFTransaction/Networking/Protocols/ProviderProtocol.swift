//
//  ProviderProtocol.swift
//  ios-transaction-sdk
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import Foundation
protocol ProviderProtocol {
    func request<T>(type: T.Type, service: ServiceProtocol, completion: @escaping (NetworkResponse<T>) -> ()) where T: Decodable
}
