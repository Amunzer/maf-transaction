//
//  MAFTransactionSdk.swift
//  ios-transaction-sdk
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import Foundation
public struct MAFTransactionSdkConfiguration {
    var accessToken:String
    var xApiKey:String
    public init( accessToken:String ,xApiKey:String) {
        self.accessToken = accessToken
        self.xApiKey = xApiKey
    }
}
public class MAFTransactionSdk {
    static var configuration: MAFTransactionSdkConfiguration!
    static let sessionProvider = URLSessionProvider()
    static var cursor: String?
    public class func getTransactionList(page : Int, completion: @escaping (Transactions?, Error?) -> ()) {
        sessionProvider.request(type: Transactions.self, service: TransactionService.transactionList(page: page, cursor: "")) { response in
            switch response {
            case let .success(transactionList):
                cursor = transactionList.next
                completion(transactionList, nil)
            case let .apiFailure(error):
                completion(nil, error)
            case let .failure(error):
                completion(nil, error as? Error)
            }
        }
    }
    public class func getNextTransactionList(page : Int, completion: @escaping (Transactions?, Error?) -> ()) {
        if cursor == nil || cursor == ""{
            cursor = nil
            let error = NSError(domain: "No more transactions", code: -99, userInfo: nil)
            completion(nil, error as Error)
            return
        }else{
            let cur = self.cursor
            self.cursor = nil
            sessionProvider.request(type: Transactions.self, service: TransactionService.transactionList(page: page, cursor: cur ?? "")) { response in
                switch response {
                case let .success(transactionList):
                    cursor = transactionList.next
                    completion(transactionList, nil)
                case let .apiFailure(error):
                    completion(nil, error)
                case let .failure(error):
                    completion(nil, error as? Error)
                }
            }
        }
    }
    public class func start(with configuration:MAFTransactionSdkConfiguration){
        MAFTransactionSdk.configuration = configuration
    }
}
