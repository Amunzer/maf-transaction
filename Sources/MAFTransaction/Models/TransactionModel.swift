//
//  TransactionModel.swift
//  ios-transaction-sdk
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import Foundation

// MARK: - TransactionElement
public struct Transactions: Codable {
    public let transactions: [TransactionModelList]?
    public let next: String?
    public let prev: String?
    public let pageSize: String?

    enum CodingKeys: String, CodingKey {
        case transactions, next, prev
        case pageSize = "page_size"
    }
}
public struct TransactionModelList: Codable {
    public let transactionDate: String
    public let list: [TransactionModel]

    enum CodingKeys: String, CodingKey {
        case transactionDate = "transaction_date"
        case list
    }
}

// MARK: - List

public struct TransactionModel: Codable {
    public let id: String?
    public let bitReference: String?
    public let title: String?
    public let subTitle: String?
    public let sponsorLogo: String?
    public let status: String?
    public let error: TransactionErrorModel?
    public let totalPoints: Int?
    public let currency: String?
    public let amount: Int?
    public let fullDate: String?
    public let transactionDate: String?
    public let transactionType: String?


    enum CodingKeys: String, CodingKey {
        case id
        case bitReference = "bit_reference"
        case title = "title"
        case subTitle = "sub_title"
        case sponsorLogo = "logo"
        case status, error
        case totalPoints = "total_points"
        case transactionType = "transaction_type"
        case currency = "currency"
        case amount = "amount"
        case transactionDate = "transaction_date"
        case fullDate = "full_date"

    }
}
// MARK: - Error
public struct TransactionErrorModel: Codable {
    public let errorDescription: String?
    public let message: String?
    public let recieptImageURL: String?

    enum CodingKeys: String, CodingKey {
        case errorDescription = "description"
        case message
        case recieptImageURL = "reciept_image_url"
    }
}
