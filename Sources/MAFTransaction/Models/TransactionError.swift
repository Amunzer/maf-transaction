//
//  TransactionError.swift
//  ios-transaction-sdk
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import Foundation
// MARK: - TransactionError
public class TransactionError: Codable, Error {
    public let code: String
    public let message: String
}
