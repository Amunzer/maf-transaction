//
//  TransactionService.swift
//  ios-transaction-sdk
//
//  Created by Husni Abu-Arqoub on 1/15/20.
//  Copyright © 2020 MAF-DK. All rights reserved.
//

import Foundation
enum TransactionService: ServiceProtocol {
    
    case transactionList(page: Int, cursor: String)
    
    var baseURL: URL {
        return URL(string: "https://maf-holding-dev.apigee.net/")!
    }
    
    var version: String {
        return "v1/"
    }
    
    var path: String {
        switch self {
        case .transactionList:
            return "gravity/dk-gravity-transactions"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .transactionList:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case let .transactionList(page, cursor):
            let parameters: [String : Any]
            if cursor.isEmpty {
                parameters = ["page_size": page]
            }else{
                parameters = ["page_size": page, "cursor": cursor]
            }
            return .requestParameters(parameters)
        }
    }
    
    var headers: Headers? {
        return ["x-api-key":MAFTransactionSdk.configuration.xApiKey,
                "Authorization":"Bearer \(MAFTransactionSdk.configuration.accessToken)"]
    }
    var parametersEncoding: ParametersEncoding {
        return .url
    }
}
